package api

// Signer is the interface that wrap the SMK SSH Signing operation.
type Signer interface {
	Init(config map[string]string) error
	Sign(req certReq) (string, error)
	ReadCA() (string, error)
}

type certReq struct {
	Key        string
	ID         string
	Principals []string
}
