package api

import (
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

func caHandler(c *gin.Context) {
	publicKey, err := config.Signer.ReadCA()
	if err != nil {
		log.Debugf("%+v", errors.Wrap(err, "error getting CA certificate"))
		log.Errorf("Error getting CA certificate: %s", errors.Cause(err))
		c.JSON(500, gin.H{"errors": "error getting CA certificate"})
		return
	}

	c.JSON(200, gin.H{"public_key": publicKey})
}
