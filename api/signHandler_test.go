package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSignHandler(t *testing.T) {
	type JSONResponse map[string]interface{}

	cases := []struct {
		method      string
		url         string
		code        int
		payload     login
		response    interface{}
		contentType string
	}{
		{"GET", "/v1/sign", 404, login{}, JSONResponse(nil), "text/plain"},
		{"PUT", "/v1/sign", 404, login{}, JSONResponse(nil), "text/plain"},
		{"PATCH", "/v1/sign", 404, login{}, JSONResponse(nil), "text/plain"},
		{"DELETE", "/v1/sign", 404, login{}, JSONResponse(nil), "text/plain"},
		{
			"POST", "/v1/sign", 400,
			login{User: "test"},
			JSONResponse{"error": "missing field(s) in signing request"},
			"application/json",
		},
		{
			"POST", "/v1/sign", 401,
			login{User: "baduser", Password: "badpassword", PubKey: "goodkey"},
			JSONResponse{"error": "login failed"},
			"application/json",
		},
		{
			"POST", "/v1/sign", 401,
			login{User: "testuser", Password: "badpassword", PubKey: "goodkey"},
			JSONResponse{"error": "login failed"},
			"application/json",
		},
		{
			"POST", "/v1/sign", 401,
			login{User: "emptyprincsuser", Password: "testpassword", PubKey: "goodkey"},
			JSONResponse{"error": "error getting list of principals"},
			"application/json",
		},
		{
			"POST", "/v1/sign", 400,
			login{User: "testuser", Password: "testpassword", PubKey: "badkey"},
			JSONResponse{"error": "unknown server error during key signing"},
			"application/json",
		},
		{
			"POST", "/v1/sign", 200,
			login{User: "testuser", Password: "testpassword", PubKey: "goodkey"},
			JSONResponse{"certificate": "goodcert"},
			"application/json",
		},
	}

	config = Config{
		Auth:   &authMock{},
		Princs: &princsMock{},
		Signer: &signerMock{},
	}
	router := setupRouter()

	for _, c := range cases {
		w := httptest.NewRecorder()
		mj, _ := json.Marshal(c.payload)
		req, _ := http.NewRequest(c.method, c.url, bytes.NewBuffer(mj))
		router.ServeHTTP(w, req)

		var response JSONResponse
		json.Unmarshal(w.Body.Bytes(), &response)

		assert.Equal(t, w.Code, c.code)
		assert.Equal(t, c.response, response)
		assert.Contains(t, w.Header().Get("Content-Type"), c.contentType)
	}
}

type authMock struct{}

func (a authMock) Login(user, password string) (bool, error) {
	if user != "testuser" && user != "emptyprincsuser" {
		return false, fmt.Errorf("unknown username")
	}

	if password != "testpassword" {
		return false, fmt.Errorf("invalid password")
	}

	return true, nil
}

func (a authMock) Init(config map[string]string) error {
	return nil
}

type princsMock struct{}

func (p princsMock) Init(config map[string]string) error {
	return nil
}

func (p princsMock) Get(user string) ([]string, error) {
	if user == "emptyprincsuser" {
		return []string{}, fmt.Errorf("empty list of principals")
	}

	return []string{"root", "user"}, nil
}

type signerMock struct{}

func (s signerMock) Init(config map[string]string) error {
	return nil
}

func (s signerMock) ReadCA() (string, error) {
	return "", nil
}

func (s signerMock) Sign(req certReq) (string, error) {
	if req.Key == "goodkey" {
		return "goodcert", nil
	}

	if req.Key == "badkey" {
		return "", fmt.Errorf("bad key format")
	}

	return "", fmt.Errorf("failed to sign key")
}
