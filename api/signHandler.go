package api

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

func signHandler(c *gin.Context) {
	var json login

	err := c.ShouldBindWith(&json, binding.JSON)
	if err != nil {
		log.Debugf("%+v", errors.Wrap(err, "JSON binding failed"))
		log.Errorf("JSON binding failed: %s", errors.Cause(err))
		c.JSON(400, gin.H{
			"error": "missing field(s) in signing request",
		})
		return
	}

	valid, err := config.Auth.Login(json.User, json.Password)
	if !valid {
		log.Debugf("%+v", errors.Wrap(err, "user login failed"))
		log.Infof("user login failed: %s", errors.Cause(err))
		c.JSON(401, gin.H{
			"error": "login failed",
		})
		return
	}

	principals, err := config.Princs.Get(json.User)
	if err != nil {
		log.Debugf("%+v", errors.Wrap(err, "Error getting user principals"))
		log.Warnf("Error getting user principals: %s", errors.Cause(err))
		c.JSON(401, gin.H{
			"error": "error getting list of principals",
		})
		return
	}

	req := certReq{
		Key:        json.PubKey,
		ID:         json.User,
		Principals: principals,
	}
	cert, err := config.Signer.Sign(req)
	if err != nil {
		log.Debugf("%+v", errors.Wrap(err, "Error signing ssh key"))
		log.Errorf("Error signing ssh key: %s", errors.Cause(err))
		c.JSON(400, gin.H{
			"error": "unknown server error during key signing",
		})
		return
	}

	c.JSON(200, gin.H{
		"certificate": cert,
	})
}
