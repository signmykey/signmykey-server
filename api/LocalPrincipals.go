package api

import (
	"fmt"
	"strings"
)

// LocalPrincipals struct represents map of principals by user.
type LocalPrincipals struct {
	UserMap map[string]string
}

// Init method is used to ingest config of LocalPrincipals
func (p *LocalPrincipals) Init(config map[string]string) error {
	p.UserMap = config

	return nil
}

// Get method is used to get the list of principals associated to a specific user.
func (p LocalPrincipals) Get(user string) ([]string, error) {

	principals := []string{}

	list, ok := p.UserMap[user]
	if !ok {
		return principals, fmt.Errorf("No principals found for %s", user)
	}

	for _, str := range strings.Split(list, ",") {
		trimmed := strings.Trim(str, " ")
		if len(trimmed) > 0 {
			principals = append(principals, trimmed)
		}
	}

	if len(principals) == 0 {
		return principals, fmt.Errorf("No more principals after trim for %s", user)
	}

	return principals, nil
}
