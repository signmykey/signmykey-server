package api

type login struct {
	User     string `json:"user" binding:"required"`
	Password string `json:"password" binding:"required"`
	PubKey   string `json:"public_key" binding:"required"`
}
