package api

import (
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

// Config represents the config of the API webserver.
type Config struct {
	VaultAddress string
	VaultToken   string
	VaultPath    string
	VaultRole    string

	TTL string

	Auth   Authenticator
	Princs Principals
	Signer Signer
}

var (
	config Config
)

// Run starts the API webserver and register all handlers.
func Run(startconfig Config) error {
	config = startconfig

	r := setupRouter()
	err := r.Run()

	return err
}

func setupRouter() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(logger())
	r.Use(gin.Recovery())

	r.GET("/v1/ping", pingHandler)
	r.POST("/v1/sign", signHandler)
	r.GET("/v1/ca", caHandler)

	return r
}

func logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := time.Now()
		c.Next()
		latency := time.Since(t)
		log.Infof(
			"GIN %s %d %s %s %v",
			c.Request.Method,
			c.Writer.Status(),
			c.Request.URL.Path,
			c.ClientIP(),
			latency)
	}
}
