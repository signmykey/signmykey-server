#!/bin/bash

vault secrets enable -path=signmykey-server-ssh ssh
vault write signmykey-server-ssh/config/ca generate_signing_key=true
vault write signmykey-server-ssh/roles/sign-user-role -<<"EOH"
{
  "allow_user_certificates": true,
  "allowed_users": "*",
  "allow_user_key_ids": true,
  "default_extensions": [
    {
      "permit-pty": ""
    }
  ],
  "key_type": "ca",
  "default_user": "root",
  "ttl": "30m0s"
}
EOH

vault auth enable -path=signmykey-users userpass
vault auth tune -max-lease-ttl=1s -default-lease-ttl=1s signmykey-users
vault write auth/signmykey-users/users/myuser password=test

vault write sys/policy/signmykey-server policy=-<<"EOH"
path "signmykey-server-ssh/config/ca" {
  capabilities = ["read"]
}

path "signmykey-server-ssh/sign/sign-user-role" {
  capabilities = ["create", "update"]
}
EOH

vault auth enable approle

vault write auth/approle/role/signmykey-server \
  token_num_uses=1 \
  token_ttl=1m \
  token_max_ttl=1m \
  policies=signmykey-server

sleep 2

vault read auth/approle/role/signmykey-server/role-id
vault write -f auth/approle/role/signmykey-server/secret-id
