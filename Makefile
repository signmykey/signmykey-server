PKG_LIST := $(shell go list ./... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
VERSION := ${VERSION}

.PHONY: all build test lint

all: build

lint_install:
	apt-get update -y
	apt-get install -y curl lbzip2
	curl -LO https://github.com/alecthomas/gometalinter/releases/download/v2.0.2/gometalinter-v2.0.2-linux-amd64.tar.bz2
	tar xf gometalinter-v2.0.2-linux-amd64.tar.bz2
	mv gometalinter-v2.0.2-linux-amd64/gometalinter gometalinter-v2.0.2-linux-amd64/linters/* /usr/local/go/bin/

lint: ## Lint the files
	gometalinter --vendor --exclude=.*_test.go --concurrency=2 --deadline=300s --disable-all --line-length=100 --enable=goimports --enable=lll --enable=misspell --enable=nakedret --enable=unparam --enable=vet --enable=vetshadow --enable=gotype --enable=deadcode --enable=gocyclo --enable=golint --enable=varcheck --enable=structcheck --enable=maligned --enable=errcheck --enable=ineffassign --enable=unconvert --enable=goconst --enable=gas --enable=unused ./...

test: ## Run unittests
	go test -short ${PKG_LIST}

build: ## Build the binary file
	go build -ldflags "-w -s -extldflags '-static' -X gitlab.com/signmykey/signmykey-server/cmd.versionString=$(VERSION)"

fpm_install:
	apt-get update -y && apt-get install ruby ruby-dev rubygems build-essential -y
	gem install --no-ri --no-rdoc fpm

fpm:
	fpm -s dir -t deb -n signmykey-server -m "contact@pablo-ruth.fr" --url "https://gitlab.com/signmykey/signmykey-server" --description "A server to sign ssh keys with Hashicorp Vault" --category "admin" -v $(VERSION) --deb-systemd signmykey-server.service --prefix /usr/bin signmykey-server

fpm_upload_dev:
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X POST -F file=@signmykey-server_$(VERSION)_amd64.deb https://apt.signmykey.io/api/files/signmykey-server_$(VERSION)
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X POST https://apt.signmykey.io/api/repos/signmykey-server-dev/file/signmykey-server_$(VERSION)
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X PUT -H 'Content-Type: application/json' --data '{"Signing": {"Skip": true}}' https://apt.signmykey.io/api/publish/signmykey-server-dev/xenial
	@curl -f -o /dev/null --silent --head https://apt.signmykey.io/signmykey-server-dev/pool/main/s/signmykey-server/signmykey-server_$(VERSION)_amd64.deb
	
fpm_upload_tag:
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X POST -F file=@signmykey-server_$(VERSION)_amd64.deb https://apt.signmykey.io/api/files/signmykey-server_$(VERSION)
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X POST https://apt.signmykey.io/api/repos/signmykey-server/file/signmykey-server_$(VERSION)
	@curl -u $(APTLY_USER):$(APTLY_PASSWORD) -X PUT -H 'Content-Type: application/json' --data '{"Signing": {"Skip": true}}' https://apt.signmykey.io/api/publish/signmykey-server/xenial
	@curl -f -o /dev/null --silent --head https://apt.signmykey.io/signmykey-server/pool/main/s/signmykey-server/signmykey-server_$(VERSION)_amd64.deb

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
