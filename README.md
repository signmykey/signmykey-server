## API

### Ping
  Returns pong message.

* **URL**

  /v1/ping

* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ message : "pong" }`

### Sign
  Returns signed public key certificate.

* **URL**

  /v1/sign/ldap

* **Method:**

  `POST`
 
* **Data Params**

  `{
    user : [string],
    password : [string],
    public_key : [string]
  }`
 
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ certificate : [string] }`

* **Error Response:**

  * **Code:** 400 <br />
    **Content:** `{ error : [string] }`
