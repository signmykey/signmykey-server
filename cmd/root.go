package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/signmykey/signmykey-server/api"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var rootCmd = &cobra.Command{
	Use:   "signmykey-server",
	Short: "A server to sign ssh keys",
	RunE: func(cmd *cobra.Command, args []string) error {
		log.Info("start signmykey-server")

		// Log level
		logLevelConfig := map[string]log.Level{
			"debug": log.DebugLevel,
			"info":  log.InfoLevel,
			"warn":  log.WarnLevel,
			"fatal": log.FatalLevel,
			"panic": log.PanicLevel,
		}
		viper.SetDefault("logLevel", "info")
		logLevel, ok := logLevelConfig[strings.ToLower(viper.GetString("logLevel"))]
		if !ok {
			log.Fatalf("invalid logLevel %s (debug,info,warn,fatal,panic)", viper.GetString("logLevel"))
		}
		log.SetLevel(logLevel)

		// Authenticator init
		authTypeConfig := viper.GetString("authenticatorType")
		if authTypeConfig == "" {
			log.Fatal("authenticator type not defined in config")
		}
		authType := map[string]api.Authenticator{
			"ldap":  &api.LDAPAuthenticator{},
			"vault": &api.VaultAuthenticator{},
		}
		auth, ok := authType[authTypeConfig]
		if !ok {
			return fmt.Errorf("unknown authenticator type %s", authTypeConfig)
		}
		err := auth.Init(viper.GetStringMapString("authenticatorOpts"))
		if err != nil {
			return err
		}

		// Principals init
		princsTypeConfig := viper.GetString("principalsType")
		if princsTypeConfig == "" {
			return fmt.Errorf("principals type not defined in config")
		}
		princsType := map[string]api.Principals{
			"local": &api.LocalPrincipals{},
			"ldap":  &api.LDAPPrincipals{},
		}
		princs, ok := princsType[princsTypeConfig]
		if !ok {
			return fmt.Errorf("unknown principals type %s", princsTypeConfig)
		}
		err = princs.Init(viper.GetStringMapString("principalsOpts"))
		if err != nil {
			return err
		}

		// Signer init
		signerTypeConfig := viper.GetString("signerType")
		if signerTypeConfig == "" {
			return fmt.Errorf("signer type not defined in config")
		}
		signerType := map[string]api.Signer{
			"vault": &api.VaultSigner{},
		}
		signer, ok := signerType[signerTypeConfig]
		if !ok {
			return fmt.Errorf("unknown signer type %s", signerTypeConfig)
		}
		err = signer.Init(viper.GetStringMapString("SignerOpts"))
		if err != nil {
			return err
		}

		config := api.Config{
			TTL:    viper.GetString("ttl"),
			Auth:   auth,
			Princs: princs,
			Signer: signer,
		}

		err = api.Run(config)

		return err
	},
}

// Execute wraps a call to Cobra execute and handles returned error
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.AddConfigPath("/etc/signmykey-server")
	viper.SetConfigName("config")

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}
}
